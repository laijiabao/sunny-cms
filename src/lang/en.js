export default {
  route: {
    user: 'User Management',
    sample: 'Sample',
    sample1: 'Sample1',
    sample2: 'Sample2'
  },
  navbar: {
    logOut: 'Log Out'
  },
  login: {
    title: 'Task Management System',
    logIn: 'Login',
    username: 'Username',
    password: 'Password'
  },
  general: {
    ID: 'ID',
    create: 'Create',
    edit: 'Edit',
    delete: 'Delete',
    detail: 'Detail',
    confirm: 'Confirm',
    cancel: 'Cancel',
    success: 'Success',
    error: 'Error',
    actions: 'Actions',
    search: 'Search'
  },
  error: {
    general: 'Network Error. Please try again',
    length: 'Input length must be {min} to {max} characters',
    password: 'Please Input password',
    passwordNotMatch: 'Two passwords not the same',
    user: {
      username: 'Please input username',
      firstName: 'Please input first name',
      lastName: 'Please input last name'
    }
  },
  user: {
    username: 'Username',
    lastLogin: 'Last Login',
    create: 'Create User',
    edit: 'Edit User',
    status: {
      status: 'Status',
      A: 'Active',
      I: 'Inactive',
      D: 'Deleted'
    },
    firstName: 'First Name',
    lastName: 'Last Name',
    fullName: 'Full Name',
    password: 'Password',
    confirmPassword: 'Confirm Password',
    email: 'Email',
    role: 'User Type',
    ip: 'IP address',
    staffId: 'staff ID'

  }
}
