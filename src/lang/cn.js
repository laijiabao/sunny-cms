export default {
  route: {
    user: '用户管理',
    sample: '更多例子',
    sample1: '例1',
    sample2: '例2'
  },
  navbar: {
    logOut: '退出登录'
  },
  login: {
    title: '任务管理系统',
    logIn: '登录',
    username: '账号',
    password: '密码'
  },
  general: {
    ID: 'ID',
    create: '新建',
    edit: '编辑',
    delete: '删除',
    detail: '详细资料',
    confirm: '确定',
    cancel: '取消',
    success: '操作成功',
    error: '错误',
    actions: '操作',
    search: '搜索'
  },
  success: {
    general: '操作成功',
    delete: '删除成功'
  },
  error: {
    general: '网络错误，请稍后再试',
    length: '输入长度在{min}到{max}之间',
    password: '请输入密码',
    passwordNotMatch: '两次密码不一致',
    email: '请输入邮箱',
    emailNotValid: '请正确输入邮箱',
    user: {
      username: '请输入用户名',
      firstName: '请输入名',
      lastName: '请输入姓'
    }
  },
  user: {
    username: '用户名',
    lastLogin: '最后登入',
    create: '新建用户',
    edit: '编辑用户',
    status: {
      status: '状态',
      A: '正常',
      I: '停用',
      D: '已删除'
    },
    firstName: '名',
    lastName: '姓',
    fullName: '姓名',
    password: '密码',
    confirmPassword: '重复密码',
    email: '邮箱',
    role: '用户类型',
    ip: 'IP地址',
    staffId: '员工ID'

  }
}
