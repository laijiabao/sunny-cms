import Vue from 'vue'
import VueI18n from 'vue-i18n'
import Cookies from 'js-cookie'
import elementEnLocale from 'element-ui/lib/locale/lang/en' // element-ui lang
import elementZhLocale from 'element-ui/lib/locale/lang/zh-TW'// element-ui lang
import elementCnLocale from 'element-ui/lib/locale/lang/zh-CN'// element-ui lang
import enLocale from './en'
import zhLocale from './hk'
import cnLocale from './cn'

Vue.use(VueI18n)

const messages = {
  en: {
    ...enLocale,
    ...elementEnLocale
  },
  hk: {
    ...zhLocale,
    ...elementZhLocale
  },
  cn: {
    ...cnLocale,
    ...elementCnLocale
  }
}

const i18n = new VueI18n({
  // set locale
  locale: Cookies.get('language') || 'cn',
  // set locale messages
  messages
})

export default i18n
